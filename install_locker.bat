@ECHO off
setlocal
CD /D D:\
ECHO ===================== BEGIN INSTALL LOCKER POPBOX ================== 
ECHO:
ECHO ___________________________CLONE GUI POPBOX _________________________
ECHO Please type GUI Folder Name ('popboxclient_ina') : 
SET /p path_project=

powershell -Command git clone https://riwandylbs@bitbucket.org/popboxdev/custom-ina.git %path_project%

CD /D D:\%path_project%

ECHO Choose BRANCH Project Name ('master as default') : 
SET /p branch=

IF %branch%=="" SET /A branch=master

powershell -Command git fetch --all
powershell -Command git checkout %branch%

ECHO ===================== CLONE BYPASS SERVICE ================== 
ECHO:
CD /D D:\

SET path_bypass=bypass-service-new
powershell -Command git clone https://riwandylbs@bitbucket.org/popboxdev/bypass-service.git %path_bypass%

CD /D D:\%path_bypass%

ECHO Choose BRANCH Project Name ('master as default') : 
SET /p branch_bypass=

IF %branch_bypass%=="" SET /A branch_bypass=master

powershell -Command git fetch --all
powershell -Command git checkout %branch_bypass%

ECHO Trying to install nodejs version 11.12.0

SET NODEJS_FILENAME=node-v11.12.0-x86.msi
SET NODEJS_URL=https://nodejs.org/dist/v11.12.0/%NODEJS_FILENAME%
SET NODEJS_DOWNLOAD_LOCATION=D:\

powershell -NoExit -Command "(New-Object Net.WebClient).DownloadFile('%NODEJS_URL%', '%NODEJS_DOWNLOAD_LOCATION%%NODEJS_FILENAME%'); exit;"
msiexec /qn /l* C:\node-log.txt /i %NODEJS_DOWNLOAD_LOCATION%%NODEJS_FILENAME%

ECHO Installing Python3 ...

CD /D D:\bypass-service

SET PYTHON_FILENAME=python-3.4.3.msi
SET PYWIN32_FILENAME=pywin32-219.win32-py3.4.exe
SET PYQT5_FILENAME=PyQt5-5.4.1-gpl-Py3.4-Qt5.4.1-x32.msi

	
START /WAIT msiexec /qn /i %PYTHON_FILENAME% ALLUSERS=1
START /WAIT msiexec /qn /i %PYQT5_FILENAME% 
START /WAIT C:\Python34\Scripts\easy_install-3.4.exe %PYWIN32_FILENAME% 

CD /C  C:\

set PATH=C:\Python34
set PATH "%PATH%;C:\Python34\Scripts"

CD /D D:\bypass-service

SET PIP=get-pip.py

SET VIDEO_CAPTURE=VideoCapture-0.9.5-cp34-none-win32.whl

python %PIP%
python -m pip install pygame==1.9.2
python -m pip install %VIDEO_CAPTURE%
python -m pip install pyqrcode
python -m pip install pypng
python -m pip install ntplib
python -m pip install pyserial==2.7

ECHO Create Task Scheduler 
SET PATH_BYPASS=D:\%path_bypass%\app\run.vbs
SET PATH_GUI=D:\%path_project%\app\run.vbs

START /WAIT SchTasks /Create /sc onstart /TN "bypass-service" /TR %PATH_BYPASS%
START /WAIT SchTasks /Create /sc onstart /TN "Autoload GUI PopBox" /TR %PATH_GUI%

ECHO ===================== FINISH, THANK YOU ================== 
endlocal
REM shutdown.exe /r /t 00
pause