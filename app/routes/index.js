const app              = module.exports = require('express')();
const axios            = require('axios')
const tb_parcel        = require('./../actions/parcel');
var spawn              = require("child_process").spawn;
var util               = require("util");

app.post('/post-data', (req, res) => {
    var paramRequest = req.body.param;
    var urlRequest = req.body.url; 
    var headerRequest = req.body.header; 
    var headers = '';

    console.log(urlRequest)
    var str_url = urlRequest 
    var checking_url_payment = str_url.search("payment");

    if (checking_url_payment > 0) {
        headers =  { 
            'Content-type': 'application/json',
        }
    } else {
        try {
            headers = {
                'Content-type': 'application/json',
                'OrderNo':  headerRequest.hasOwnProperty('OrderNo') ? headerRequest.OrderNo : null,
                'DiskSerialNumber': headerRequest.hasOwnProperty('DiskSerialNumber') ? headerRequest.DiskSerialNumber : null,
                'BoxToken': headerRequest.hasOwnProperty('BoxToken') ? headerRequest.BoxToken : null
            }
        } catch (error) {
            res.status(200).send({
                code: 500,
                message: error.message
            })
        }
    }

    curl_request(urlRequest, paramRequest, headers, function(response) {
        return res.send(response)
    });
});  

var curl_request = function(url, param, headers, callback){
    axios.post(url, param,{
        headers: headers
    })
    .then(response =>{
        console.log(param)
        callback(response.data)
    }).catch(err =>{
        console.error(err)
    });
}

app.get('/express/all', (req, res) => {
    var sql = `SELECT * FROM Express order by storeTime desc`;
    tb_parcel.expressData(sql, function (response) {
        return res.send(response);
    });
});

app.get('/', (req, res) => {
    return res.send("Welcome To Bypass System Locker");
});

app.post('/express/filter', (req, res) => {
    var expressNumber = req.body.expressNumber;
    console.log(req.body.expressNumber);
    var sql = `SELECT * FROM Express where expressNumber = '` + expressNumber +`'`;
    tb_parcel.expressData(sql, function (response) {
        console.log(response);
        return res.send(response);
    });
});


app.post('/open-door', (req, res) => {
    var id = req.body.id
    var open_type = req.body.type

    try {
        var sql = ""
        if (open_type == "OPEN_PARCEL") {
            sql = `SELECT Mouth.number, Mouth.numberInCabinet FROM Express inner join Mouth on Express.mouth_id = Mouth.id where Express.id = '`+ id +`' AND Express.status = 'IN_STORE'`;
        } else {
            sql = `SELECT number, numberInCabinet FROM Mouth WHERE id = '`+ id +`'`
        }
        
        tb_parcel.expressData(sql, function (response) {
            try {
                var door = response[0]
                console.log(door)
                var args = JSON.stringify({
                    "type" : "OPEN_DOOR",
                    "data" : {
                        "number" : door.number,
                        "numberInCabinet" : door.numberInCabinet
                    }
                })
                const message = "Door is Open"
                python_shell(args, res, message);
            } catch (error) {
                return res.status(200).send({
                    code : 500,
                    message : error.message
                })
            }
        });
    } catch (error) {
        return res.status(200).send({
            code : 500,
            message : error.message
        })
    }
   
});

function python_shell(args, res, message=null) {
    var process = spawn('python',["./python_shell.py", args]);
    util.log('readingin.....!')
    process.stdout.on('data',function(chunk){
        console.log(chunk)
        var textChunk = chunk.toString('utf8'); // buffer to string
        util.log(textChunk);
        return res.status(200).send({
            code : 200,
            message : message
        })
    });
}

app.get('/restart', (req, res) => {
    try {
        var args = JSON.stringify({
            "type" : "RESTART",
        })
        const message = "Restarting Computer"
        python_shell(args, res, message)
    } catch (error) {
        return res.status(500).send({
            code : 500,
            message : error.message
        })
    }
})

app.all('*', (req, res) => {
    res.status(404).send('not found');
})
